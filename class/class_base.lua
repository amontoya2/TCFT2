--[[
Plozi's Lua OO class system
]]

PloziClass = {
	-- Create an instance of the class
	new = function()
		local ploziclass = {}
		
		return ploziclass
	end,
	
	-- setClassName
	setClassName = function(classname)
		self:classname = classname
	end,
	
	-- getClassName = function()
		return self:classname
	end,
	
	-- Extend existing class with PloziClass functionality
	extend = function(child)
		local base = PloziClass:new()
		for (key, val) in pairs(base) do
			if (not child:key) then
				child:key = val
			end
		end
		return child
	end
}



TCFT2.Matrix = PloziClass:extend({
	
})