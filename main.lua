--[[
TCFT2
(c) By Plozi
and DimmyR
** Changelog
2017-09-28 - added ircRelay function, fixed apostrophe in TC2 chat
2017-10-18 - bugfix for relay crashing the client
]]

TCFT2 = TCFT2 or {}
TCFT2.Version = "1.59.1"
TCFT2.ChatType = 1 -- Default to tc2 chat
TCFT2.LastPrivMsgUser = ""
TCFT2.Binds = {} -- Keyboard binds
TCFT2.IdString = "opeaOAzep329dnroa882l3kweuwhsendw83gbsyaa"
--TCFT2.IdString = "opxaOAzep319dnroa881l3kweuwhsdndw93gbsyaa"
TCFT2.ServerVersion = "0.0.0"
TCFT2.Server = "vo.aoczone.net"
TCFT2.debug = 0
TCFT2.StatusList = {} -- Table with status messages in main TCFT2 chat
TCFT2.GuildOnlineTimer = Timer()
TCFT2.PDASecInfo = nil
TCFT2.StationSecInfo = nil
TCFT2.CapshipSecInfo = nil
TCFT2.IsInitialised = false

TCFT2.SettingsSaveId = 81922705660

TCFT2.GuildMembers = {} -- Should contain GuildMembers[name] = rank - used to display users here and there...

TCFT2.UserOptions = {} -- Options from server for current logged in user

-- Have to define targetless somehow to be able to check it...
targetless = targetless or {}
targetless.api = targetless.api or {}
targetless.api.radarlock = targetless.api.radarlock or false
targetless.var = targetless.var or {}
targetless.var.scanlock = targetless.var.scanlock or false

-- Load sounds
gksound.GKLoadSound{soundname='tcft2sound1',filename='plugins/tcft2/sound/bipp01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound2',filename='plugins/tcft2/sound/bipp02.ogg'}
gksound.GKLoadSound{soundname='tcft2sound3',filename='plugins/tcft2/sound/bipp03.ogg'}
gksound.GKLoadSound{soundname='tcft2sound4',filename='plugins/tcft2/sound/bipp04.ogg'}
gksound.GKLoadSound{soundname='tcft2sound5',filename='plugins/tcft2/sound/bipp05.ogg'}
gksound.GKLoadSound{soundname='tcft2sound6',filename='plugins/tcft2/sound/bipp01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound7',filename='plugins/tcft2/sound/boing02.ogg'}
gksound.GKLoadSound{soundname='tcft2sound8',filename='plugins/tcft2/sound/boing03.ogg'}
gksound.GKLoadSound{soundname='tcft2sound9',filename='plugins/tcft2/sound/bipp01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound10',filename='plugins/tcft2/sound/charge01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound11',filename='plugins/tcft2/sound/dill01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound12',filename='plugins/tcft2/sound/doorbell01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound13',filename='plugins/tcft2/sound/oiik01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound14',filename='plugins/tcft2/sound/tada01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound15',filename='plugins/tcft2/sound/trideli01.ogg'}
gksound.GKLoadSound{soundname='tcft2sound16',filename='plugins/tcft2/sound/zapp01.ogg'}

TCFT2.Sounds = {}
TCFT2.Sounds['Click'] = 'click';
TCFT2.Sounds['Explosion'] = 'explosion';
TCFT2.Sounds['Gauss'] = 'gauss';
TCFT2.Sounds['Laser'] = 'laser';
TCFT2.Sounds['Mine'] = 'mine';
TCFT2.Sounds['Mouseover'] = 'mouseover';
TCFT2.Sounds['Powerup'] = 'powerup';
TCFT2.Sounds['Prox Warning'] = 'prox.warning';
TCFT2.Sounds['Zapp'] = 'bipp1';
TCFT2.Sounds['TCFT2 01'] = 'tcft2sound1';
TCFT2.Sounds['TCFT2 02'] = 'tcft2sound2';
TCFT2.Sounds['TCFT2 03'] = 'tcft2sound3';
TCFT2.Sounds['TCFT2 04'] = 'tcft2sound4';
TCFT2.Sounds['TCFT2 05'] = 'tcft2sound5';
TCFT2.Sounds['TCFT2 06'] = 'tcft2sound6';
TCFT2.Sounds['TCFT2 07'] = 'tcft2sound7';
TCFT2.Sounds['TCFT2 08'] = 'tcft2sound8';
TCFT2.Sounds['TCFT2 09'] = 'tcft2sound9';
TCFT2.Sounds['TCFT2 10'] = 'tcft2sound10';
TCFT2.Sounds['TCFT2 11'] = 'tcft2sound11';
TCFT2.Sounds['TCFT2 12'] = 'tcft2sound12';
TCFT2.Sounds['TCFT2 13'] = 'tcft2sound13';
TCFT2.Sounds['TCFT2 14'] = 'tcft2sound14';
TCFT2.Sounds['TCFT2 15'] = 'tcft2sound15';
TCFT2.Sounds['TCFT2 16'] = 'tcft2sound16';

TCFT2.SoundLabels = {}

for key, val in pairs(TCFT2.Sounds) do
	table.insert(TCFT2.SoundLabels, key)
end

table.sort(TCFT2.SoundLabels)
table.insert(TCFT2.SoundLabels, 1, "None")

-- Load other files
dofile("tcpsock.lua")
dofile("json.lua")
dofile("common.lua")
dofile("class/matrix.class.lua")
dofile("events.lua")
dofile("connection.lua")
dofile("trading.lua")
dofile("mining.lua")
dofile("help.lua")
dofile("cargolist.lua")
dofile("settings.lua")
dofile("inventory.lua")
dofile("guild.lua")
dofile("comdata.lua")
dofile("navigation.lua")
dofile("lastseen.lua")
dofile("players.lua")
dofile("ui/ui_main.lua")
dofile("relay/relay.lua")

-- Selected tab
TCFT2.CurMainTab = TCFT2.TCFTTab

TCFT2.DebugPrint = function(msg)
	console_print(msg)
end

-- AutoAccept the aula
local eula_original_show = EULADialog.show_cb

function EULADialog:show_cb()
  eula_original_show(EULADialog)
  local old_eula = gkini.ReadString("Vendetta", "EULA", SHA1(GetEULA()))
  local new_eula = SHA1(GetEULA())
  if old_eula == new_eula then
    EULADialog[1][1][3][1][2][2].action() -- this is the "Accept" button
  end
  gkini.WriteString("Vendetta", "EULA", new_eula)
end

local function get_args(str)
	local quotechar
	local i=0
	local args,argn,rest={},1,{}
	while true do
		local found,nexti,arg = string.find(str, '^"(.-)"%s*', i+1)
		if not found then found,nexti,arg = string.find(str, "^'(.-)'%s*", i+1) end
		if not found then found,nexti,arg = string.find(str, "^(%S+)%s*", i+1) end
		if not found then break end
		table.insert(rest, string.sub(str, nexti+1))
		table.insert(args, arg)
		i = nexti
	end
	return args,rest
end


-- Main command - display TCFT2 window
TCFT2.Command = function(_, args)
	if (args) then
		local numargs = #args
		local cmd = string.lower(args[1])

		local subcommand = string.lower(args[2] or "")
		
		-- Asteriod Scanner commands
		if (cmd=="scanner") then
			if (subcommand=="on") then -- turn scanner on
				TCFT2.Mining.Scanner.Start()
			elseif (subcommand=="off") then -- turn scanner off
				TCFT2.Mining.Scanner.Stop()
			elseif (subcommand=="toggle") then -- toggle scanner on/off
				TCFT2.Mining.Scanner.Toggle()
			end
		elseif (cmd=="test") then
			-- table.insert(TCFT2.LastSeen.Notifications, { time=os.time(), txt="Testing Plozi", label=nil })
			for id, name in pairs(SystemNames) do
				console_print(id .. " - " .. name)
			end
		elseif (cmd=="cargolist") then
			if (not PlayerInStation()) then
				TCFT2.DisplayCargolist()
			end
		elseif (cmd=="autoeject") then
			if (subcommand=="on") then -- turn autoeject on
				TCFT2.Settings.Data.AutoEjectActive = "ON"
			elseif (subcommand=="off") then -- turn autoeject off
				TCFT2.Settings.Data.AutoEjectActive = "OFF"
			elseif (subcommand=="toggle") then -- toggle autoeject mode
				TCFT2.Settings.Data.AutoEjectActive = (TCFT2.Settings.Data.AutoEjectActive=="ON" and "OFF") or "ON"
			end
			TCFT2.AutoEjectActive.value = TCFT2.Settings.Data.AutoEjectActive
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "AutoEject turned " .. TCFT2.Settings.Data.AutoEjectActive, true)
		elseif (cmd=="navedit") then
			TCFT2.PloziNav.Editor.ShowEditor()
		elseif (cmd=="navreload") then
			TCFT2.PloziNav.LoadServerNavData()
		elseif (cmd=="connect") then
			TCFT2.Connection.CheckConnection()
		elseif (cmd=="lastseen") then
			local name = ""
			for i=2,numargs do name = name .. " " .. (args[i] or "") end
			name = TCFT2.trim(name)
			TCFT2.DisplayLastSeenList(name)
		elseif (cmd=="saveclist") then
			TCFT2.Trading.SaveCargoHold()
		elseif (cmd=="loadclist") then
			TCFT2.Trading.LoadCargoHold()
		else
			print(TCFT2.colors.YELLOW .. "Unknown command: " .. cmd)
		end
	else
		TCFT2.DisplayTCFT2()
	end
end

-- CleanUp - leaving
TCFT2.CleanUp = function()
	-- Do other needed cleanup
	TCFT2.Trading.UpdateCargoXP() -- Update XP if catched after last docking
	-- TCFT2.Settings.Save()
	TCFT2.Connection.CleanUp()
	-- TCFT2.Trading.UpdateStationCargo()
end

-- Read Settings from ini-file
TCFT2.LoadSettings = function()
	-- TCFT2.Settings.Data = unspickle(gkini.ReadString('TCFT2','Settings',''))
	TCFT2.Settings.Data = unspickle(LoadSystemNotes(TCFT2.SettingsSaveId))
	TCFT2.Settings.Data.Scanner = TCFT2.Settings.Data.Scanner or {}
	TCFT2.Settings.Data.AutoEjectKeep = TCFT2.Settings.Data.AutoEjectKeep or {}
	TCFT2.Settings.Data.Binds = TCFT2.Settings.Data.Binds or {}
	TCFT2.Settings.Update()
end

-- Save Settings to ini-file
TCFT2.SaveSettings = function()
	-- gkini.WriteString('TCFT2', 'Settings', spickle(TCFT2.Settings.Data))
	SaveSystemNotes(spickle(TCFT2.Settings.Data), TCFT2.SettingsSaveId)
end

-- Set status on the statusline
TCFT2.EventSetStatusLine = function(_, msg)
	if (TCFT2.statusline.title) then
		TCFT2.statusline.title = msg
	end
end

-- Set status line - trigger event
TCFT2.SetStatusLine = function(txt)
	ProcessEvent("TCFT2_SETSTATUSLINE", txt)
end
--LisaJunk = {}
-- Send data to server - trigger event for sending
TCFT2.SendData = function(data)
	if (TCFT2.Connection.isLoggedin ~= true) and (data.cmd ~= "100") then
		TCFT2.AddStatus(TCFT2.colors.RED .. "Error sending data - not logged in.")
	else
--print("====>"..json.encode(data).."<====")
		TCFT2.Connection:Send(json.encode(data) .. "\r\n")
		--LisaJunk = json.encode(data)
	end
end

TCFT2.SubmitTargetLessRoidDb = function()
	if (TCFT2.Connection.isLoggedin) then
		if (targetless) then
			local tldb = unspickle(LoadSystemNotes(targetless.var.noteoffset) or "") or {}
			--printtable(tldb) lisa un-commented it out
			--printtable(tldb)
			for sectorid, data in pairs(tldb) do
				local data2 = unspickle(data)
				for objectid, ores in pairs(data2) do
					local roid = { sectorid=sectorid, objectid=objectid, ores={} }
					for name, density in pairs(ores.ore) do
						--console_print("Ore: " .. name .. " -> Dens: " .. tonumber(density)) lisa  uncommented it.
						--print("Ore: " .. name .. " -> Dens: " .. tonumber(density))
						table.insert(roid.ores, { orename=name, density=density } )
					end
					TCFT2.Mining.RoidBuffer[objectid] = roid
					TCFT2.Mining.RoidInfo[objectid] = roid
				end
			end
		end
		TCFT2.SendData({ cmd="500" })
	else
		TCFT2.ErrorDialog("Error", "You are not logged in.")
	end
end

-- Trim text
TCFT2.trim = function(txt)
	txt = txt or ""
	return (string.gsub(txt, "^%s*(.-)%s*$", "%1"))
end

-- Escape apostrophes
TCFT2.escapeApostrophes = function (txt)
	txt = txt or ""
	return (string.gsub(txt, "\'", "\""))
end

-- Add text to main TCFT status window
TCFT2.AddStatus = function(statusmsg, chatecho)
	if (statusmsg~="") then
		table.insert(TCFT2.StatusList, statusmsg)
	end
	if (#TCFT2.StatusList>100) then
		table.remove(TCFT2.StatusList, 1)
	end
	if (TCFT2.txt_status~=nil) then
		TCFT2.txt_status.value = table.concat(TCFT2.StatusList, "\n")
		if (TCFT2.Settings.Data.chatecho=="ON" and chatecho) then
			print(statusmsg .. TCFT2.colors.NORMAL)
		end
		if (TCFT2.CurMainTab~=TCFT2.TCFTTab) then
			TCFT2.MainTabs:SetTabTextColor(2, "255 0 0")
		end
	end
end

-- TCFT Chat
TCFT2.DoSendTCFTChatMessage = function(msg)
	local locstr = ""
	local sectorid = -1
	msg = TCFT2.escapeApostrophes(msg)
	if (TCFT2.Settings.Data.broadcastlocation=="ON") then
		if (GetCurrentSectorid() ~= nil) then
			sectorid = GetCurrentSectorid()
		else
			sectorid = -1
		end
	end
	TCFT2.SendData({ cmd="200", msg=msg, sectorid=sectorid })
end

-- TCFT Chat - Say from cmd-line in VO
--plugins/tcft2/main.lua:322: bad argument #1 to 'ipairs' (table expected, got nil)
--error from droidbuttons having a button set to say_tc2
--Looks like it expects the message also... not just the say_tc2

TCFT2.TCFTSay = function(data,args)
	local txt = ""
	for _,arg in ipairs(args) do
		txt = txt .. " " .. arg
	end
	TCFT2.DoSendTCFTChatMessage(txt)
end

TCFT2.SetChatType = function(type)
	TCFT2.ChatType	= type
	TCFT2.ChatTypeRadio.value = TCFT2.rdo_chattype[type]
	local chatlabel
	if (type==1) then
		chatlabel = TCFT2.colors.GREEN2 .. "TCFT:"
	else 
		chatlabel = TCFT2.colors.GUILD_ORANGE .. "Guild:"
	end
	TCFT2.chatlabel.title = chatlabel
	-- iup.SetFocus(TCFT2.txt_tcftchatline)
end

-- TCFT/Guild Chat - from TCFT UI
TCFT2.TCFTChat = function()
	-- Helper function
	local msg = TCFT2.txt_tcftchatline.value
	local txt = TCFT2.trim(msg)
	if (string.len(txt)>0) then
		if string.byte(txt, 1) == iup.K_slash then
			local cmd,rest = get_args(txt)
			if (cmd[1]=="/msg") then
				local recipient, message = substitute_vars(cmd[2]), substitute_vars(rest[2])
				if message and message ~= '' then
					SendChat(message, "PRIVATE", recipient)
					ProcessEvent("CHAT_MSG_PRIVATEOUTGOING", { name=recipient, msg=message })
				end
			elseif (cmd[1]=="/who") then
				TCFT2.SendData({ cmd="150" })
			elseif (cmd[1]=="/whoguild") then
				if ((GetGuildName() or "")=="") then
					TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.GUILD_ORANGE .. "You are not in a guild.")
				else
					TCFT2.ShowOnlineGuildMembers(true)
				end
			elseif (cmd[1]=="/clear") then
				TCFT2.StatusList = {}
				TCFT2.AddStatus("")
			elseif (cmd[1]=="/help") then
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "TCFT2 Commands:")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/who - displays logged in users.")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/whoguild - displays guild members online.")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/version - displays current TCFT2 version.")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/clear - clear message window.")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/msg user message - send message to user as a private message.")
				TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "/submit-tl - Submits your targetless roid db to TCFT2")
			elseif (cmd[1]=="/version") then
				TCFT2.AddStatus(TCFT2.colors.YELLOW .. "*** TCFT2 version " .. TCFT2.Version)
			elseif (cmd[1]=="/submit-tl") then
				TCFT2.AddStatus(TCFT2.colors.YELLOW .. "*** Submutting Targetless Roid DB.")
				TCFT2.SubmitTargetLessRoidDb()
			elseif (cmd[1]=="/test") then
				TCFT2.AddStatus(TCFT2.colors.YELLOW .. "*** TEST")
			elseif (cmd[1]=="/connect") then
				TCFT2.Connection.CheckConnection()
			elseif (cmd[1]=="/me") then
				if (TCFT2.ChatType == 2) then -- guild chat
					SendChat(txt, 'GUILD')
				else -- tcft chat
					TCFT2.DoSendTCFTChatMessage(txt)
				end
			elseif (cmd[1]=="/guildbank") then
				--Guild.getbanklogpage(1)
				if (Guild.getactivitylogpage(1000000)~=nil) then
					TCFT2.MessageDialog("Guild", "Error")
				end
			else
				TCFT2.AddStatus(TCFT2.colors.RED .. "Unknown command: " .. cmd[1] .. TCFT2.colors.NORMAL)
			end
		else
			if (TCFT2.ChatType == 2) then -- guild chat
				SendChat(txt, 'GUILD')
			else -- tcft chat
				TCFT2.DoSendTCFTChatMessage(txt)
			end
		end
	end
	TCFT2.txt_tcftchatline.value = ""
end

---- **** EVENTS **** ----

-- Guild chat
TCFT2.EventGuildChat = function(event, params)
	local faccolor = FactionColor_RGB[params.faction]
	local locstr = string.format(" [%s] ", ShortLocationStr(params.location))
	local fullmsg = string.format(TCFT2.colors.GUILD_ORANGE .. "(guild)%s<%s%s%s> %s", locstr, rgbtohex(faccolor), params.name, TCFT2.colors.GUILD_ORANGE, params.msg)
	TCFT2.AddStatus(fullmsg)
end

-- Guils Emote
TCFT2.EventGuildEmote = function(event, params)
	local faccolor = FactionColor_RGB[params.faction]
--	local locstr = string.format(" [%s] ", ShortLocationStr(params.sectorid))
	local locstr = string.format(" [%s] ", ShortLocationStr(params.location))
	local fullmsg = string.format(TCFT2.colors.GUILD_ORANGE .. "(guild)%s**%s%s%s %s", locstr, rgbtohex(faccolor), params.name, TCFT2.colors.GUILD_ORANGE, params.msg)
	TCFT2.AddStatus(fullmsg)
end

-- Guild member logs on or off
TCFT2.EventGuildMember = function(event, charid)
	if (TCFT2.Settings.Data.displayguildlog=="ON") then
		if (event=='GUILD_MEMBER_ADDED') then
			local charid, rank, name = GetGuildMemberInfoByCharID(charid)
			local membername = (TCFT2.GuildRank[tonumber(rank)] or "") .. " " .. name
			local msg = TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.GUILD_ORANGE .. "(guild) *** " .. membername .. " logged on." .. TCFT2.colors.NORMAL
			TCFT2.AddStatus(msg, true)
		elseif (event=='GUILD_MEMBER_REMOVED') then
			local msg = TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.GUILD_ORANGE .. "(guild) *** " .. GetPlayerName(charid) .. " logged off." .. TCFT2.colors.NORMAL
			TCFT2.AddStatus(msg, true)
		end
	end
end

-- Private message event
TCFT2.EventPrivMessage = function(event, params)
	local msg = ""
	if (event=='CHAT_MSG_PRIVATE') then
		msg = TCFT2.colors.RED .. '*' .. TCFT2.colors.NORMAL .. params.name .. TCFT2.colors.RED .. '* ' .. params.msg .. TCFT2.colors.NORMAL
		TCFT2.LastPrivMsgUser = params.name
		TCFT2.AddStatus(msg)
	elseif (event=='CHAT_MSG_PRIVATEOUTGOING') then
		msg = TCFT2.colors.RED .. '->' .. TCFT2.colors.NORMAL .. params.name .. TCFT2.colors.RED .. ': ' .. params.msg .. TCFT2.colors.NORMAL
		TCFT2.AddStatus(msg)
	end
end

-- Entering a station - do required stuff
TCFT2.EventEnteredStation = function(_, params)
	if (HasActiveShip()) then -- Only try to repair and refill if we have an active ship
		if ((TCFT2.Settings.Data.autorepair or "ON")=="ON") then -- Auto repair ship
			RepairShip(GetActiveShipID(), 1)
			--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Ship repaired." .. TCFT2.colors.NORMAL, true)
		end
		if ((TCFT2.Settings.Data.autorefill or "ON")=="ON") then -- Auto refill ship
			--ReplenishAll(GetActiveShipID())
			for j = 2, GetActiveShipNumAddonPorts()-1, 1 do
				if GetInventoryItemName(GetActiveShipItemIDAtPort(j)) then
					local current, maximum= GetAddonItemInfo(GetActiveShipItemIDAtPort(j))
					if current < maximum then
						ReplenishWeapon(GetActiveShipItemIDAtPort(j),maximum - current)
					end
				end
			end
			--TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Ship refilled with ammo." .. TCFT2.colors.NORMAL, true)
		end
	end
end

-- Leaving station - do required stuff
TCFT2.EventLeaveStation = function(_, params)

end

-- Entering a new sector
TCFT2.EventSectorChanged = function(_, params) -- You are entering a new sector
	-- print("New sector: " .. LocationStr(params))
end

-- Display guildmembers online
TCFT2.ShowOnlineGuildMembers = function(onlytcft)
	if ((GetGuildName() or "")~="") then
		local broadcast = true
		if (onlytcft~=nil) then broadcast = false end
		local members = ""
		for i=1,GetNumGuildMembers() do
			local charid, rank, name = GetGuildMemberInfo(i)
			if (members~="") then members = members .. ", " end
			local membername
			--[[
			local tname = string.lower(name)
			if ((TCFT2.GuildMembers[tname] or 0)<1) then -- probie
				membername = TCFT2.colors.RED .. "(Probie) " .. name .. TCFT2.colors.NORMAL
			else
				membername = TCFT2.colors.GUILDORANGE .. (TCFT2.GuildRank[tonumber(rank)] or "") .. " " .. name .. TCFT2.colors.NORMAL
			end
			]]
			membername = TCFT2.colors.GUILD_ORANGE .. (TCFT2.GuildRank[tonumber(rank)] or "") .. " " .. name .. TCFT2.colors.NORMAL
			members = members .. membername
		end
		members = ((members~="" and members) or "None")
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.GUILD_ORANGE .. "(guild) *** Guild members online: " .. members, broadcast)
	end
	--if (PlayerInStation()) and (TCFT2.Connection.isLoggedin==true) then
	--	if (TCFT2.ServerVersion~="0.0.0") and (TCFT2.ServerVersion~=TCFT2.Version) then
	--		TCFT2.ErrorDialog("Version Mismatch", "There is a newer version of TCFT2 available for download.\n\nCheck change-log and Download at http://vo.aoczone.net/download")
	--	end
	--end
end

-- create random string for password
function TCFT2.RandomString(length)
if length < 1 then return nil end
local valChars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
length = length or 1
local array = ""
for i = 1,length do
	local A = math.random(#valChars)
	array = array..string.sub(valChars,A,A)
end
return array
end

-- Player enters game
TCFT2.EventPlayerEnteredGame = function(_, params)
	if TCFT2.IsInitialised == false then
		SetShipPurchaseColor(229)
		local dpasswd = gkini.ReadString("TGFT_Util","Password" ,"None")
		if ((dpasswd == "None") or (string.len(dpasswd) < 20)) then 
			dpasswd = TCFT2.RandomString(30)
			gkini.WriteString("TGFT_Util", "Password", dpasswd)
		end
		urlopen('http://www.tgft.org/vendetta/stats.php', 'POST',nil, {username=GetPlayerName(GetCharacterID()), passwd=dpasswd})
		TCFT2.InitButtons() -- Initialize TCFT2 buttons
		TCFT2.LoadSettings() -- Load settings - second thing
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.YELLOW .. "Version " .. TCFT2.Version .. " loaded.", true)
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Command is /tc2", true)
		TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Type /help in TCFT2 chat for available commands.", true)
		if (TCFT2.Settings.Data.autoconnect=="ON") then
			TCFT2.AddStatus(TCFT2.colors.GREEN2 .. "(TCFT2) " .. TCFT2.colors.NORMAL .. "Automatic connection on. Trying to connect...", true)
			TCFT2.Connection.CheckConnection()
		end
		TCFT2.Trading.UpdateShipCargo()
		TCFT2.Mining.AddInfoField() -- Add the mining info field (roid info)
		TCFT2.IsInitialised = true
		TCFT2.relay.relay_init()
	else
	end
end

TCFT2.GeneralEvent = function(event, params)
	console_print("XXEvent: " .. event)
end

TCFT2.UpdateAccomplishments = function(event, params)
	--printtable(params)
end

-- Abs function
TCFT2.abs = function(num)
	return num >= 0 and num or -num
end

-- Testin
local function p(msg)
	TCFT2.AddStatus(msg)
end

local function print_r(x)
	for k,v in pairs(x) do
		if (type(v)~="table" and type(v)~="boolean") then
			p(k.." = " .. v)
		end
	end
end

TCFT2.Test = function()
	TCFT2.Trading.SubmitStation()
end

RegisterUserCommand('tc2', TCFT2.Command)
RegisterUserCommand('say_tc2', TCFT2.TCFTSay)
RegisterUserCommand('tc2_connect', TCFT2.Connection.CheckConnection)

-- Player entered game - do initialize stuff
RegisterEvent(TCFT2.EventPlayerEnteredGame, "PLAYER_ENTERED_GAME")

-- Interface unloading
RegisterEvent(TCFT2.CleanUp, "UNLOAD_INTERFACE") --Testing to see if this causes the 'it && key' failed. nope.

RegisterEvent(TCFT2.EventEnteredStation, "ENTERED_STATION")
RegisterEvent(TCFT2.Mining.EventEnteredStation, "ENTERED_STATION")
RegisterEvent(TCFT2.Trading.EventEnteredStation, "ENTERED_STATION")

RegisterEvent(TCFT2.EventLeaveStation, "LEAVING_STATION")
RegisterEvent(TCFT2.Mining.EventLeaveStation, "LEAVING_STATION")
RegisterEvent(TCFT2.Trading.EventLeaveStation, "LEAVING_STATION")


-- Guild chat
RegisterEvent(TCFT2.EventGuildChat,"CHAT_MSG_GUILD")
RegisterEvent(TCFT2.EventGuildEmote,"CHAT_MSG_GUILD_EMOTE")

RegisterEvent(TCFT2.EventGuildMember, "GUILD_MEMBER_ADDED")
RegisterEvent(TCFT2.EventGuildMember, "GUILD_MEMBER_REMOVED")

-- Private message in/out
RegisterEvent(TCFT2.EventPrivMessage,"CHAT_MSG_PRIVATE")
RegisterEvent(TCFT2.EventPrivMessage,"CHAT_MSG_PRIVATEOUTGOING")

-- Entering a new sector
RegisterEvent(TCFT2.EventSectorChanged, "SECTOR_CHANGED")
RegisterEvent(TCFT2.Trading.EventSectorChanged, "SECTOR_CHANGED")
RegisterEvent(TCFT2.Mining.EventSectorChanged, "SECTOR_CHANGED")
RegisterEvent(TCFT2.CargoList.EventSectorChanged, "SECTOR_CHANGED")

-- Target scan/change
RegisterEvent(TCFT2.Mining.EventTargetScanned, "TARGET_SCANNED")
RegisterEvent(TCFT2.Mining.EventTargetChanged, "TARGET_CHANGED")

RegisterEvent(TCFT2.Mining.EventInventoryChange, "INVENTORY_ADD")
RegisterEvent(TCFT2.Mining.EventInventoryChange, "INVENTORY_REMOVE")
RegisterEvent(TCFT2.Mining.EventInventoryChange, "INVENTORY_RECEIVED")
RegisterEvent(TCFT2.Mining.EventInventoryChange, "WEAPON_PROGRESS_START")
RegisterEvent(TCFT2.Mining.EventInventoryChange, "WEAPON_PROGRESS_STOP")

RegisterEvent(TCFT2.Trading.EventInventoryAdd, "INVENTORY_ADD")
RegisterEvent(TCFT2.Trading.EventInventoryRemove, "INVENTORY_REMOVE")
RegisterEvent(TCFT2.Trading.EventInventoryUpdate, "INVENTORY_UPDATE")
RegisterEvent(TCFT2.Trading.EventChatSectordMission, "CHAT_MSG_SECTORD_MISSION")
RegisterEvent(TCFT2.Trading.EventTransactonComplete, "TRANSACTION_COMPLETED")

-- Rem'd these out due to a error exiting VO.  Lisa
--RegisterEvent(TCFT2.GeneralEvent, "START")
--RegisterEvent(TCFT2.GeneralEvent, "QUIT")
--RegisterEvent(TCFT2.GeneralEvent, "TERMINATE")
--RegisterEvent(TCFT2.GeneralEvent, "UNLOAD_INTERFACE")

--[[
Event: INVENTORY_REMOVE - Data:
	37
Event: CHAT_MSG_SECTORD_MISSION - Data:
	missionid=0,msg="Trading/Commerce +8"
]]

-- UnregisterEvent(HUD,"TARGET_SCANNED")

-- TCFT2 Events
RegisterEvent(TCFT2.EventSetStatusLine, "TCFT2_SETSTATUSLINE")

--RegisterEvent(TCFT2.UpdateAccomplishments, "PLAYER_UPDATE_ACCOMPLISHMENTS")

