--[[
TCFT2 - Common stuff
]]


-- Hex color codes '\127RRGGBB' format
TCFT2.colors = {}
TCFT2.colors.YELLOW = '\127FFFF00'
TCFT2.colors.NORMAL = '\127DDDDDD'
TCFT2.colors.ORANGE = '\127AAAA00'
TCFT2.colors.GUILD_ORANGE = '\127ffb935'
TCFT2.colors.RED = '\127FF0000'
TCFT2.colors.INDIAN_RED = '\127CD5C5C'
TCFT2.colors.GREEN = '\127008800'
TCFT2.colors.GREEN2 = '\12733CC33'
TCFT2.colors.BLUE = '\1270000FF'
TCFT2.colors.DRK_GREEN = '\127888800'
TCFT2.colors.BLACK = "\127000000"
TCFT2.colors.GREY1 = '\127808080'
TCFT2.colors.GREY2 = '\127AAAAAA'
TCFT2.colors.GREY3 = '\127CCCCCC'

-- Regular color codes, "R G B" format.
TCFT2.colors.RGBBLUE = "0 0 127"
TCFT2.colors.RGB_RED = "127 0 0"
TCFT2.colors.RGB_DRK_GREY = "153 153 153"  -- GREY60
TCFT2.colors.LT_GREY = "127 127 127"  -- .GREY50
TCFT2.colors.RGB_GREY = "110 110 110" -- GREY43
TCFT2.colors.CENT_MARKER = "107 142 35" --olivedrab
TCFT2.colors.RGB_BLACK = '00 00 00'

TCFT2.itemtypesraw = {
	turret=0,
	commodities=1,
	ship=2,
	lightweapon=3,
	heavyweapon=3,
	battery=4
}

TCFT2.itemtypes = {
	Turret=0,
	Commodities=1,
	Ships=2,
	Weapons=3,
	Batteries=4
}

TCFT2.itemtypesrev = {
	[-1] = "???",
	[0] = "Turret",
	[1] = "Commodity",
	[2] = "Ship",
	[3] = "Weapon",
	[4] = "Battery",
	[5] = "Turret"
	
}

TCFT2.GuildRank = {
	[0] = "",
	[1] = "(Lieutenant)",
	[2] = "(Council)",
	[3] = "(Council/Lieutenant)",
	[4] = "(Commander)"
}

TCFT2.SystemNames = {}
TCFT2.Stations = {}

for i=1,#SystemNames do
	if (SystemNames[i]~="Devlopia") then
		table.insert(TCFT2.SystemNames, SystemNames[i])
	end
end

table.sort(TCFT2.SystemNames)

-- printtable(SystemNames)

-- General message
TCFT2.msg = function(str)
	print("\127ffb935TCFT2: \127ffffff" .. str)
end

-- Error message
TCFT2.error = function(str)
	print("\127ffb935TCFT2: \127ff0000" .. str)
end


-- Subtabs
TCFT2.subtabs = function(params)
	local curtab = params[1]
	params.buttonselimage=IMAGE_DIR.."tab.button_selected.png"
	params.buttonunselimage=IMAGE_DIR.."tab.button_unselected.png"
	params.buttonmouseoverimage=IMAGE_DIR.."tab.button_mouseover.png"
	params.buttonimagecenteruv=string.format("%f %f %f %f", 10/32, 10/32, 10/32, 10/32)
	params.buttonimageuv=string.format("0 0 %f %f", 21/32, 21/32)
	params.buttonimageglowborder="4"
	params.alignment = "ATOP"
	params.selimage1 = IMAGE_DIR.."pda_sub_sub_tab_sel.png"
	params.unselimage1 = IMAGE_DIR.."pda_sub_sub_tab_unsel.png"
	params.image1centeruv = "0.5 0.625 0.5 0.625"
	params.selimage2 = IMAGE_DIR.."pda_sub_sub_tab_sel.png"
	params.unselimage2 = IMAGE_DIR.."pda_sub_sub_tab_unsel.png"
	params.image2centeruv = "0.5 0.625 0.5 0.625"
	params.upperleftimage = IMAGE_DIR.."pda_sub_sub_tab_left.png"
	params.upperleftsegments = "0 0 0 0"
	params.upperrightimage = IMAGE_DIR.."pda_sub_sub_tab_right.png"
	params.upperrightsegments = "1 0 1 0"
	params.upperrightsize = "0x0"
	params.borderimagename = IMAGE_DIR.."pda_sub_sub_tab_border.png"
	params.borderimagesegments = "0.265625 0.171875 0.734375 0.734375"  -- "8.5 5.5 23.5 23.5"
	params.tabbuttonoverlap = "0x-2"
	params.secondary = iup.hbox{ iup.fill { size=5}, iup.vbox { iup.fill{size=5},iup.stationbutton{title="Help (F1)", hotkey=iup.K_F1, tip="Help for this Tab", action=function() curtab:OnHelp() end}}, iup.fill{}}
	params.tabchange_cb = function(self, newtab, oldtab)
		curtab = newtab
		if (oldtab.OnDeactivate) then
			oldtab:OnDeactivate(newtab, oldtab)
		end
		if (newtab.OnActivate) then
			newtab:OnActivate(newtab, oldtab)
		end
	end
	local onactivate = params.OnActivate or nil
	local ondeactivate = params.OnDeactivate or nil
	params.OnActivate = nil
	params.OnDeactivate = nil
	local ret = iup.pda_sub_tabs(params)
	ret.OnActivate = onactivate
	ret.OnDeactivate = ondeactivate
	return ret
end
