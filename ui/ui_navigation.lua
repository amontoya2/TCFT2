--[[

Navigation computer UI

]]


TCFT2.PloziNav.UI = TCFT2.PloziNav.UI or {}
TCFT2.PloziNav.UI.buttons = {}
TCFT2.PloziNav.UI.buttons.PlotRoute = {}
TCFT2.PloziNav.UI.EditDialog = nil


--[[
PloziNav UI stuff
]]
-- PlotRoute buttons - need one for each NavTab
TCFT2.PloziNav.UI.CreatePanel = function()
	local btn = iup.stationbutton {
		title = "Plot route",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		hotkey=iup.K_p,
		action = function(self) TCFT2.PloziNav.PlotRoute() end,
	}
	local repeatbtn = iup.stationbutton {
		title = "Replot Last Route",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self) TCFT2.PloziNav.RepeatLast() end,
	}
	
	local panel = iup.hbox {
		iup.fill {},
		btn,
		iup.fill { size=10 },
		repeatbtn,
		iup.fill {},
		margin="4x4",
	}
	return panel
end

--[[
Route editor stuff
]]

-- Create the editor dialog
TCFT2.PloziNav.UI.create_edit_dialog = function()
	local dlg, container, buttons
	local yres = gkinterface.GetYResolution() - 300
	
	TCFT2.PloziNav.UI.navmap = iup.navmap{
		-- size=(yres-250).."x"..(yres-250), 
		size="%50x%60",
		expand="YES", 
		ALIGNMENT="ACENTER" ,
		mouseover_cb = function(self, index, str)
			TCFT2.PloziNav.Editor.Map_Mouseover(index, str)
		end,
		click_cb = function(self, index, modifiers)
			TCFT2.PloziNav.Editor.Map_Click(index, modifiers)
		end,
	}
	
	local cancelbtn = iup.stationbutton {
		title = "Close",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.CloseEditor()
		end
	}

	local zoombtn = iup.stationbutton {
		title = "Zoom to system",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.Zoom(self)
		end,
		hotkey=iup.K_z,
	}

	TCFT2.PloziNav.UI.Savebtn = iup.stationbutton {
		title = "Save Plotted",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.SaveRoute(self)
		end,
		active="NO",
	}
	
	TCFT2.PloziNav.UI.SendToServerBtn = iup.stationbutton {
		title = "Send",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.SendDataToServer(self)
		end,
		active="NO"
	}
	
	TCFT2.PloziNav.UI.DeleteRouteBtn = iup.stationbutton {
		title = "Delete",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.DeleteRoute(self)
		end,
	}

	TCFT2.PloziNav.UI.ResetRouteBtn = iup.stationbutton {
		title = "Refresh",
		ALIGNMENT="ACENTER",
		EXPAND="NO",
		action = function(self)
			TCFT2.PloziNav.Editor.RefreshNavData(self)
		end,
	}

	TCFT2.PloziNav.UI.RouteList = iup.stationhighopacitysublist{
		expand='YES',
		size='x150',
		action = function(text, index, selection)
			TCFT2.PloziNav.Editor.RouteClick(text, index, selection)
		end,
		control="NO",
	}

	buttons = iup.vbox {
		margin="15x0",
		iup.vbox {
			margin="3x3",
			iup.label { title="Local route data" },
			iup.hbox {
				iup.hbox {
					margin="2x0",
					TCFT2.PloziNav.UI.Savebtn,
				},
				iup.hbox {
					margin="2x0",
					TCFT2.PloziNav.UI.DeleteRouteBtn,
				},
			},
		},
		iup.fill { size="2" },
		iup.vbox {
			margin="3x3",
			iup.label { title="Server data" },
			iup.hbox {
				iup.hbox {
					margin="2x0",
					TCFT2.PloziNav.UI.SendToServerBtn,
				},
				iup.hbox {
					margin="2x0",
					TCFT2.PloziNav.UI.ResetRouteBtn,
				},
			},
		},
		iup.fill { size="15" },
		iup.vbox {
			margin="3x3",
			iup.hbox {
				iup.hbox {
					iup.hbox {
						margin="2x0",
						zoombtn,
					},
				},
				iup.hbox {
					iup.hbox {
						margin="2x0",
						cancelbtn,
					},
				},
			},
		},
	}
	
	TCFT2.PloziNav.Editor.SectorInfo = iup.stationsubmultiline { value="", expand="YES", readonly="YES", size="%13x", bgcolor="30 30 30" }
	
	container =	iup.vbox {
		iup.label { title="TCFT2 Navigation Route Editor", font=Font.H2*HUD_SCALE, alignment="ACENTER", expand="YES", margin="10x15" },
		iup.hbox {
			margin="10x10",
			iup.vbox {
				TCFT2.PloziNav.UI.navmap,
			},
			iup.vbox {
				TCFT2.PloziNav.Editor.SectorInfo,
			},
		},
		iup.hbox {
			iup.vbox {
				iup.label { title="Routes for selected system", alignment="ALEFT", expand="YES" },
				TCFT2.PloziNav.UI.RouteList,
			},
			iup.vbox {
				size="%25x",
				buttons,
			},
		},
	}
	
	dlg = iup.dialog{
		iup.vbox {
			margin="%10x%10",
			iup.stationhighopacityframe {
				iup.stationhighopacityframebg {
					container,
				},
			}
		},
		bgcolor="40 40 40 192 *",
		border="NO",menubox="NO",resize="NO",
		defaultesc=cancelbtn,
		topmost="YES",
		modal="YES",
		fullscreen="YES",
		-- size=(yres+150).."x"..(yres+100),
	}

	function dlg:show_cb()
		TCFT2.PloziNav.Editor.LoadMap()
		TCFT2.PloziNav.Editor.LoadSystemRoutes()
	end

	dlg:map()
	return dlg
end

TCFT2.PloziNav.UI.EditDialog = TCFT2.PloziNav.UI.create_edit_dialog()

TCFT2.PloziNav.Editor.CloseEditor = function()
	-- TODO Check for unsaved data and confirm the close
	--TCFT2.PloziNav.msg("Dialog: " .. TCFT2.PloziNav.UI.EditDialog)
	TCFT2.PloziNav.UI.EditDialog:hide()
end

