-- TCFT2 UI
-- Trade Tab

TCFT2.Tools = TCFT2.Tools or {}

-- ITEM LOOKUP
local function createItemlookupTab()
	local container

	TCFT2.Trading.LookupList = TCFT2.MatrixList:new()
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname" }, 20))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("sellprice", "Station Sell Price", "ARIGHT", { "N:A:sellprice" }, 9, { postfix=" c" }))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("buyprice", "Station Buy Price", "ARIGHT", { "N:D:buyprice" }, 9, { postfix=" c" }))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("xp", "XP", "ARIGHT", { "N:D:xp" }, 6, { replace={ ["0"]="--" }, postfix=" xp" }))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("location", "Location", "ALEFT", { "S:A:location" }, 15))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("station", "Station", "ALEFT", { "S:A:station" }, 15))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("context", "Context", "ALEFT", { "S:A:context" }, 15))
	TCFT2.Trading.LookupList:addcolumn(TCFT2.MatrixList.column.new("dateupdated", "Last Updated", "ARIGHT", { "S:D:dateupdated" }, 11))
	TCFT2.Trading.LookupList:init()
	
	TCFT2.Trading.LookupItemType = iup.stationsublist { "Anything", "Commodities", "Ships", "Weapons", "Batteries"; dropdown='yes', size='%10x', expand='NO', visible_items=15 }
	TCFT2.Trading.LookupItemType.action = TCFT2.Trading.LookupItemTypeChanged
	TCFT2.Trading.LookupItemList = iup.stationsublist { dropdown='yes', size='%20x', expand='NO', visible_items=15 } -- visible_items=25 to 15 -- tokkana
	TCFT2.Trading.LookupBoughtSold = iup.stationsublist { "Bought and Sold", "Bought", "Sold", "Wanted"; dropdown='yes', size='%10x', expand='NO', visible_items=5 }
	TCFT2.Trading.LookupSystem = iup.stationsublist { "Any system"; dropdown="YES", size="%15x", expand="NO", visible_items=15 } -- visible_items=25 to 15 -- tokkana
	TCFT2.Trading.TradeLookupButton = iup.stationbutton { 
		title="Lookup",
		action = function(self)
			TCFT2.Trading.DoTradeLookup()
		end,
		active="YES"
	}
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Item lookup", font=Font.H3 }, iup.fill {},
		},
		iup.fill { size="5" },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.LookupItemType,
			TCFT2.Trading.LookupItemList,
			TCFT2.Trading.LookupBoughtSold,
			iup.label { title=" in "},
			TCFT2.Trading.LookupSystem,
			TCFT2.Trading.TradeLookupButton,
			gap="5",
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.LookupList,
		},
	}
	
	container.tabtitle = "Item lookup"
	container.hotkey = iup.K_l
	function container:OnHelp()
	   TCFT2.ShowHelp("TRADING_ITEMLOOKUP")
	end
	function container:OnActivate(newtab, oldtab)
		if (TCFT2.Connection.isLoggedin==true) then
			TCFT2.SendData({ cmd="400" }) -- Update available trade items
		end
	end
	return container
end

-- STATION COMPARE
local function createStationCompareTab()
	local container

	TCFT2.Trading.StationCompareList = TCFT2.MatrixList:new()
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname" }, 22))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("type", "Item Type", "ALEFT", { "S:A:type" }, 8))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("buylocation", "Buy at", "ALEFT", { "S:A:buylocation", "S:A:selllocation" }, 15))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("sellprice", "Buy Price", "ARIGHT", { "N:A:sellprice" }, 7, { postfix=" c" })) -- Station sell price - OUR buy price
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("selllocation", "Sell at", "ALEFT", { "S:A:selllocation", "S:A:buylocation" }, 15))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("buyprice", "Sell Price", "ARIGHT", { "N:D:buyprice" }, 7, { postfix=" c" })) -- Station buy price - OUR sell price
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("volume", "Volume", "ARIGHT", { "N:A:volume" }, 6, { postfix=" cu" }))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("mass", "Mass", "ARIGHT", { "N:A:mass" }, 7, { postfix=" Kg" }))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("xp", "XP", "ARIGHT", { "N:D:xp" }, 6, { replace={ ["0"]="--" }, postfix = " xp" }))
	TCFT2.Trading.StationCompareList:addcolumn(TCFT2.MatrixList.column.new("cuprofit", "Profit/CU", "ARIGHT", { "N:D:cuprofit" }, 7, { postfix=" c" }))
	TCFT2.Trading.StationCompareList:init()
	
	TCFT2.Trading.StationCompareStation1 = iup.stationsublist { dropdown='yes', size='%17x', expand='NO', visible_items=15, sort="YES" } -- visible_items=25 to 15 -- tokkana
	TCFT2.Trading.StationCompareStation2 = iup.stationsublist { dropdown='yes', size='%17x', expand='NO', visible_items=15, sort="YES" } -- visible_items=25 to 15 -- tokkana
	
	TCFT2.Trading.CompareButton = iup.stationbutton { 
		title="Compare",
		action = function(self)
			TCFT2.Trading.DoStationCompare()
		end,
		active="YES"
	}
	TCFT2.Trading.SwapAndCompareButton = iup.stationbutton { 
		title="Swap And Compare",
		action = function(self)
			local tmp = TCFT2.Trading.StationCompareStation1.value
			TCFT2.Trading.StationCompareStation1.value = TCFT2.Trading.StationCompareStation2.value
			TCFT2.Trading.StationCompareStation2.value = tmp
			TCFT2.Trading.DoStationCompare()
		end,
		active="YES"
	}

	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Station Compare", font=Font.H3 }, iup.fill {},
		},
		iup.fill { size="5" },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.LookupBoughtSold,
			iup.label { title=" Buy in "},
			TCFT2.Trading.StationCompareStation1,
			iup.label { title=" Sell in "},
			TCFT2.Trading.StationCompareStation2,
			TCFT2.Trading.CompareButton,
			TCFT2.Trading.SwapAndCompareButton,
			gap="5",
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.StationCompareList,
		},
	}
	
	container.tabtitle = "Station Compare"
	container.hotkey = iup.K_s
	function container:OnHelp()
	   TCFT2.ShowHelp("TRADING_STATIONCOMPARE")
	end
	function container:OnActivate(newtab, oldtab)
		if (TCFT2.Connection.isLoggedin==true) then
			TCFT2.SendData({ cmd='450'}) -- Update stationlist
		end
	end
	return container
end


-- Trade routes
local function createTradeRoutesTab()
	local container

	TCFT2.Trading.TradeRouteList = TCFT2.MatrixList:new()
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname" }, 21))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("xp", "XP", "ARIGHT", { "S:D:xp" }, 7, { replace={ ["0"]="--" }, postfix = " xp" }))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("cuprofit", "Profit / CU", "ARIGHT", { "N:D:cuprofit" }, 9, { postfix = " c", format="%0.1f" }))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("buylocation", "Buy At", "ALEFT", { "S:A:buylocation" }, 20))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("selllocation", "Sell at", "ALEFT", { "S:A:selllocation" }, 20))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("hops", "Hops", "ARIGHT", { "N:A:hops" }, 5))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("volume", "Volume", "ARIGHT", { "N:A:volume" }, 9, { postfix=" cu" }))
	TCFT2.Trading.TradeRouteList:addcolumn(TCFT2.MatrixList.column.new("mass", "Mass", "ARIGHT", { "N:A:mass" }, 9, { postfix=" Kg" }))
	TCFT2.Trading.TradeRouteList:init()
	TCFT2.Trading.TradeRouteList:set_sortcol(3)
	
	TCFT2.Trading.TradeRouteStation1 = iup.stationsublist { dropdown='yes', size='%17x', expand='NO', visible_items=15, sort="YES" } -- visible_items=25 to 15 -- tokkana
	TCFT2.Trading.TradeRouteStation2 = iup.stationsublist { dropdown='yes', size='%17x', expand='NO', visible_items=15, sort="YES" } -- visible_items=25 to 15 -- tokkana
	
	TCFT2.Trading.TradeRouteSearchMinProfit = iup.stationsublist {"4000", "3000", "2000", "1000", "500"; dropdown='yes', expand='NO', sort="NO" }
	TCFT2.Trading.TradeRouteSearchSkipShips = iup.stationtoggle{ value="NO", title="No Ships" }
	TCFT2.Trading.TradeRouteSearchSkipWeps = iup.stationtoggle{ value="NO", title="No Weapons" }
	TCFT2.Trading.TradeRouteSearchSkipBatts = iup.stationtoggle{ value="NO", title="No Batteries" }
	
	TCFT2.Trading.TradeRouteSearchBtn = iup.stationbutton { 
		title="Search for routes",
		action = function(self)
			TCFT2.Trading.TradeRouteSearch()
		end,
		active="YES"
	}
	TCFT2.Trading.SwapRouteAndSearchBtn = iup.stationbutton { 
		title="Swap and Search",
		action = function(self)
			local tmp = TCFT2.Trading.TradeRouteStation1.value
			TCFT2.Trading.TradeRouteStation1.value = TCFT2.Trading.TradeRouteStation2.value
			TCFT2.Trading.TradeRouteStation2.value = tmp
			TCFT2.Trading.TradeRouteSearch()
		end,
		active="YES"
	}

	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Search for Trade Routes", font=Font.H3*HUD_SCALE }, iup.fill {},
		},
		iup.fill { size="5" },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.LookupBoughtSold,
			iup.label { title=" From "},
			TCFT2.Trading.TradeRouteStation1,
			iup.label { title=" To "},
			TCFT2.Trading.TradeRouteStation2,
			TCFT2.Trading.TradeRouteSearchBtn,
			TCFT2.Trading.SwapRouteAndSearchBtn,
			gap="5",
		},
		iup.fill { size=5 },
		iup.hbox {
			expand="YES",
			margin="5x0",
			iup.label { title="Minimum Profit / CU" },
			iup.fill { size=4 },
			TCFT2.Trading.TradeRouteSearchMinProfit,
			iup.fill { size=10 },
			TCFT2.Trading.TradeRouteSearchSkipShips,
			iup.fill { size=10 },
			TCFT2.Trading.TradeRouteSearchSkipWeps,
			iup.fill { size=10 },
			TCFT2.Trading.TradeRouteSearchSkipBatts,
		},
		iup.fill { size=10 },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.TradeRouteList,
		},
	}
	
	container.tabtitle = "Trade Routes"
	container.hotkey = iup.K_r
	function container:OnHelp()
	   TCFT2.ShowHelp("TRADING_TRADEROUTES")
	end
	function container:OnActivate(newtab, oldtab)
		if (TCFT2.Connection.isLoggedin==true) then
			TCFT2.SendData({ cmd='450'}) -- Update stationlist
		else
			TCFT2.Trading.TradeRouteSearchUpdateStations()
		end
	end
	return container
end

-- Stations that need update
local function createStationsTab()
	local container

	TCFT2.Trading.StationUpdateList = TCFT2.MatrixList:new()
	TCFT2.Trading.StationUpdateList:addcolumn(TCFT2.MatrixList.column.new("location", "Location", "ALEFT", { "S:A:location" }, 25))
	TCFT2.Trading.StationUpdateList:addcolumn(TCFT2.MatrixList.column.new("stationname", "Station Name", "ALEFT", { "S:A:stationname" }, 25))
	TCFT2.Trading.StationUpdateList:addcolumn(TCFT2.MatrixList.column.new("itemname", "Item Name", "ALEFT", { "S:A:itemname" }, 25))
	TCFT2.Trading.StationUpdateList:addcolumn(TCFT2.MatrixList.column.new("dateupdate", "Last Updated", "ARIGHT", { "S:A:dateupdate" }, 12))
	TCFT2.Trading.StationUpdateList:addcolumn(TCFT2.MatrixList.column.new("username", "Updated By", "ALEFT", { "S:A:username" }, 13))
	TCFT2.Trading.StationUpdateList:set_sortcol(4)
	TCFT2.Trading.StationUpdateList:init()
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Stations that might needs an update", font=Font.H3*HUD_SCALE }, iup.fill { size="10" },
			iup.stationbutton { 
				title="Refresh List",
				action = function(self)
					if (TCFT2.Connection.isLoggedin) then
						TCFT2.Trading.FetchUpdateStations()
					else
						TCFT2.ErrorDialog("Stations", "You are not logged in.")
					end
				end,
				active="YES"
			},
		},
		iup.fill { size="5" },
		iup.hbox {
			expand="YES",
			TCFT2.Trading.StationUpdateList,
		},
	}

	container.tabtitle = "Stations"
	container.hotkey = iup.K_i
	function container:OnHelp()
	   TCFT2.ShowHelp("TRADING_STATIONS")
	end
	return container
end


-- Trading tools

local function calculatePrice(resField)
	local itemname = TCFT2.Tools.Inventorylist[TCFT2.Tools.Inventorylist.value] or ""
	if (itemname=="") then
		resField.value="???"
		return
	end
	local itemid = TCFT2.Tools.InventoryHash[itemname]
	if (not itemid) then
		resField.value="???"
		return
	end
	local value
	if (resField==TCFT2.Tools.SellRes1) then
		value = tonumber(TCFT2.Tools.SellCalc1.value) or 0
	else
		value = tonumber(TCFT2.Tools.SellCalc2.value) or 0
	end
	if (value==0) then
		resField.value="???"
		return
	end
	local onepcs = GetStationSellableInventoryPriceByID(itemid, 1)
	local info = GetStationSellableInventoryInfoByID(itemid)
	local costprice = tonumber(info.unitcost)
	local profitonepcs = onepcs - costprice
	local num = 1
	local prev = -99999999
	for i=1, 1001 do
		local curr = GetStationSellableInventoryPriceByID(itemid, i) / i
		if (i==1) and (prev==curr) then -- Static price - can sell unlimited
			resField.value="[S] ~"
			return
		end
		local profit = curr - costprice
		local percent = profit/profitonepcs*100
		if (resField==TCFT2.Tools.SellRes1) then
			if (profit<=value) then
				resField.value = (i-1)
				return
			end
		else
			if (percent<=value) then
				resField.value = (i-1)
				return
			end
		end
		prev = curr
	end
	resField.value = "1000+"
end

TCFT2.Tools.UpdatePriceOnePcs = function()
	local itemname = TCFT2.Tools.Inventorylist[TCFT2.Tools.Inventorylist.value] or ""
	if (itemname=="") then return end
	local itemid = TCFT2.Tools.InventoryHash[itemname]
	if (not itemid) then return end
	local onepcs = GetStationSellableInventoryPriceByID(itemid, 1)
	local info = GetStationSellableInventoryInfoByID(itemid)
	local profit = tonumber(onepcs) - tonumber(info.unitcost)
	local profitcolor = TCFT2.colors.GREEN2
	if (profit<0) then
		profitcolor = TCFT2.colors.RED
	end
	TCFT2.Tools.SellPriceOnePcs.title = onepcs .. "c (profit " .. profitcolor .. profit .. TCFT2.colors.NORMAL .. "c)"
	TCFT2.Tools.SellRes1.value=""
	TCFT2.Tools.SellRes2.value=""
	TCFT2.Tools.SellCalc1.value=""
	TCFT2.Tools.SellCalc2.value=""
end

local function createToolsTab()
	local container
	
	
	TCFT2.Tools.Inventorylist = iup.stationsublist { dropdown='yes', size='%17x', expand='NO', visible_items=15, sort="YES", action=TCFT2.Tools.UpdatePriceOnePcs } -- visible_items=25 to 15 -- tokkana
	TCFT2.Tools.SellPriceOnePcs = iup.label { title="", expand="NO", alignment="ALEFT", size="220x" }
	TCFT2.Tools.SellCalc1 = iup.text { value="", expand="NO", size="60x", alignment="ARIGHT" }
	TCFT2.Tools.SellCalc2 = iup.text { value="", expand="NO", size="60x", alignment="ARIGHT" }
	TCFT2.Tools.SellRes1 = iup.text { value="", expand="NO", size="70x", alignment="ARIGHT", readonly="YES", canfocus="NO", border="NO" }
	TCFT2.Tools.SellRes2 = iup.text { value="", expand="NO", size="70x", alignment="ARIGHT", readonly="YES", canfocus="NO", border="NO" }
	
	container = iup.vbox {
		margin="4x4",
		iup.hbox {
			iup.label { title="Trade Tools", font=Font.H3*HUD_SCALE }, iup.fill {},
		},
		iup.fill { size=10 },
		iup.label { title="Price calculator"},
		iup.fill { size=5 },
		iup.hbox {
			iup.label { title="Item " }, TCFT2.Tools.Inventorylist, iup.fill { size=10 }, iup.label { title="Price when selling 1 pcs: " }, TCFT2.Tools.SellPriceOnePcs, 
		},
		iup.fill { size=10 },
		iup.vbox {
			iup.hbox {
				iup.label { title="Numbers to sell to get at least" },
				TCFT2.Tools.SellCalc1,
				iup.label { title="c profit for each item.", size ="180x"},
				iup.stationbutton { 
					title="Calculate",
					action = function(self)
						calculatePrice(TCFT2.Tools.SellRes1)
					end,
				},
				iup.label { title="Items to sell: " }, TCFT2.Tools.SellRes1,
				gap=4,
			},
		},
		iup.fill { size=10 },
		iup.vbox {
			iup.hbox {
				iup.label { title="Numbers to sell to get at least" },
				TCFT2.Tools.SellCalc2,
				iup.label { title="% profit of max price", size ="180x"},
				iup.stationbutton { 
					title="Calculate",
					action = function(self)
						calculatePrice(TCFT2.Tools.SellRes2)
					end,
				},
				iup.label { title="Items to sell: " }, TCFT2.Tools.SellRes2,
				gap=4,
			},
		},
	}
	container.tabtitle = "Tools"
	container.hotkey = iup.K_o
	function container:OnHelp()
	   TCFT2.ShowHelp("TRADING_TOOLS")
	end
	function container:OnActivate(newtab, oldtab)
		TCFT2.Tools.InventoryHash = {}
		TCFT2.Tools.Inventorylist[1] = nil
		local idx = 1
		for _,info in StationSellableInventoryPairs() do 
			if (info.type~=nil) and (TCFT2.Tools.InventoryHash[info.name]==nil) then
				TCFT2.Tools.Inventorylist[idx] = info.name
				TCFT2.Tools.InventoryHash[info.name] = info.itemid
				idx = idx + 1
			end
		end
		TCFT2.Tools.Inventorylist.value=1
		TCFT2.Tools.UpdatePriceOnePcs()
	end

	return container
end
			
TCFT2.CreateTradeTab = function()
	local ItemLookupTab = createItemlookupTab()
	local StationCompareTab = createStationCompareTab()
	local TradeRoutesTab = createTradeRoutesTab()
	local StationsTab = createStationsTab()
	local ToolsTab = createToolsTab()
	
    return TCFT2.subtabs { StationCompareTab, ItemLookupTab, TradeRoutesTab, StationsTab, ToolsTab,
    	OnActivate = function(self, newtab, oldtab)
    		if (TCFT2.Connection.isLoggedin==true) then
				TCFT2.SendData({ cmd='450'}) -- Update stationlist
			end
    	end
    }
end
